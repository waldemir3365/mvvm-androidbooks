package br.com.metasix.nyapp.presentation

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import br.com.metasix.nyapp.R
import br.com.metasix.nyapp.data.model.Book
import kotlinx.android.synthetic.main.item_book.view.*

class BooksAdapter(private val books:List<Book>) : RecyclerView.Adapter<BooksAdapter.BooksViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, view: Int): BooksViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.item_book,parent,false)
        return BooksViewHolder(itemView)
    }

    override fun getItemCount() = books.count()

    override fun onBindViewHolder(viewholder: BooksViewHolder, position: Int) {
        viewholder.bindView(books[position])
    }

    class BooksViewHolder(view: View) : RecyclerView.ViewHolder(view){

       private val title = view.textTitle
       private val author = view.textAutor

        fun bindView(book:Book){

            title.text = book.title
            author.text = book.author
        }
    }
}