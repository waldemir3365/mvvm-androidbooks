package br.com.metasix.nyapp.presentation

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import br.com.metasix.nyapp.R
import br.com.metasix.nyapp.data.model.Book
import kotlinx.android.synthetic.main.activity_books.*

class BooksActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_books)

        init()
    }

    private fun init(){
        setToolBar()
        configViewModel()
    }

    private fun configViewModel() {

        val viewModel: BooksViewModel = ViewModelProviders.of(this).get(BooksViewModel::class.java)
        viewModel.booksLiveData.observe(this, Observer {
            it?.let { books -> configRecyclerView(books) }
        })
        viewModel.getBooks()
    }

    private fun configRecyclerView(books: List<Book>) {
        with(recycler_books) {
            layoutManager = LinearLayoutManager(this@BooksActivity, RecyclerView.VERTICAL, false)
            setHasFixedSize(true)
            adapter = BooksAdapter(books)
        }
    }

    private fun setToolBar() {
        toobar.title = getString(R.string.books_title)
        setSupportActionBar(toobar)
    }

}
