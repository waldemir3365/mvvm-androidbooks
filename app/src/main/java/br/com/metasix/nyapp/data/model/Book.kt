package br.com.metasix.nyapp.data.model

data class Book(val title:String, val author:String){}