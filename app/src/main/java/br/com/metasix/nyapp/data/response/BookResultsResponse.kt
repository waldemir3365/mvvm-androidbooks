package br.com.metasix.nyapp.data.response

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class BookResultsResponse(

    @Json(name="book details")
    val bookDetails: List<BookDetails>
) {
}