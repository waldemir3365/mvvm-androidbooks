package br.com.metasix.nyapp.data

import br.com.metasix.nyapp.data.response.BookBodyResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface NYTServices {

    @GET("lists.json")
    fun getBooks(
        @Query("api-key") apikey: String = "8A7AwBKXTXZPeHxF5rJTC6mTzECp0EQB",
        @Query("list")list:String = "hardcover-fiction"
    ): Call<BookBodyResponse>
}