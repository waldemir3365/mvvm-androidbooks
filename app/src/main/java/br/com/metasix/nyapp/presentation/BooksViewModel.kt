package br.com.metasix.nyapp.presentation

import android.util.Log
import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import br.com.metasix.nyapp.data.ApiService
import br.com.metasix.nyapp.data.model.Book
import br.com.metasix.nyapp.data.response.BookBodyResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class BooksViewModel : ViewModel() {

    val booksLiveData: MutableLiveData<List<Book>> = MutableLiveData()

    fun getBooks(){
        ApiService.service.getBooks().enqueue(object:Callback<BookBodyResponse>{
            override fun onResponse(call: Call<BookBodyResponse>, response: Response<BookBodyResponse>) {
                if(response.isSuccessful){

                    val books:MutableList<Book> = mutableListOf()

                    response.body()?.let { bookBodyResponse ->

                        for(results in bookBodyResponse.bookResults){
                            val book = Book(
                                title = results.bookDetails[0].title,
                                author = results.bookDetails[0].author
                            )

                            books.add(book)
                        }
                    }
                    Log.e("mostrou","tamanho :$books.size")
                    booksLiveData.value = books
                }
            }


            override fun onFailure(call: Call<BookBodyResponse>, t: Throwable) {
            }


        })
    }

}